package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

type PodInfo struct {
	Hostname      string `json:"HOSTNAME"`
	Home          string `json:"HOME"`
	AppEnvSecret  string `json:"APP_ENV_SECRET"`
	AppEnvValue   string `json:"APP_ENV_VALUE"`
	AppFileConfig string `json:"APP_FILE_CONFIG"`
	OsRelease     string `json:"OS_RELEASE"`
}

func TestGetPodInfo(t *testing.T) {
	podInfo := PodInfo{}
	resp, err := http.Get("http://localhost:8888/")
	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode)

	err = json.NewDecoder(resp.Body).Decode(&podInfo)
	if err != nil {
		t.Error(err)
	}

	fmt.Printf("%+v", podInfo)
}

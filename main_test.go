package main

import (
	"net/http"
	"os"
	"testing"
	"time"
)

func TestMain(m *testing.M) {
	router := mainRouter()
	srv := &http.Server{Addr: ":8888", Handler: router}
	go srv.ListenAndServe()
	defer srv.Close()
	time.Sleep(100 * time.Millisecond)
	code := m.Run()
	os.Exit(code)
}
